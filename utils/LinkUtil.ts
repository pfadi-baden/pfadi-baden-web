export class LinkUtil {
    static isExternalLink (link: string): boolean {
        return link.startsWith('http');
    }
}