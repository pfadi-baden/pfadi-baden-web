// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'Pfadi Baden',
      charset: 'utf-8',
      htmlAttrs: {
        lang: 'de'
      },
      meta: [
        { hid: 'lang', name: 'lang', content: 'de' },
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Sagen dir Schnitzeljagde, Zeltstädte, Lagerfeuer und Theater zu? Entdecke unter Freunden dein nächstes Abenteuer.' },
        { name: 'format-detection', content: 'telephone=no' },
        {
          property: 'og:title',
          content: `Pfadi Baden`,
        },
        {
          property: 'og:type',
          content: `website`,
        },
        {
          property: 'og:description',
          content: `Pfadi Baden`,
        },
        // {
        //   property: 'og:image',
        //   content: 'https://test.hochwacht.ch/img/logo-lilie.png',
        // },

        // { name: 'msapplication-TileColor', content: '#ec7e1e' },
        // { name: 'theme-color', content: '#da532c' },
      ],

    }
  },

  devtools: { enabled: true },

  vite: {
    css: {
      preprocessorOptions: {
        scss: {
        }
      }
    }
  },

  runtimeConfig: {
    public: {
      apiUrl: 'http://localhost:1338/api',
      backendUrl: 'http://localhost:1337'
    },
  },

  css: ['@/public/scss/main.scss'],

  modules: [
    '@nuxtjs/robots',
  ],

  compatibilityDate: '2024-10-16',
})