import type { RichTextInput } from "../strapi/RichTextBlock";

export interface BlockSection {
    id: number;
    __component: 'layout.section';
    title: string;
    content: RichTextInput;
}

export interface MarkdownSection {
    id: number;
    __component: 'layout.markdown-section';
    title: string;
    content: string;
}