import type { BlockSection, MarkdownSection } from "./components/section";
import type { Address } from "./strapi/components/Address";
import type { Contact } from "./strapi/components/Contact";
import type { Document } from "./strapi/components/resource-center/Document";
import type { RichTextInput } from "./strapi/RichTextBlock";
import type { StrapiMedia } from "./strapi/StrapiMedia";

export interface Building {
    description: RichTextInput;
    images: StrapiMedia[];
    isRentable: boolean;
    name: string;
    Contact: Contact;
    Address: Address;
    Documents: Document[];
    // An array of block sections and markdown sections
    sections: (BlockSection | MarkdownSection)[];
    thumbnail: StrapiMedia;

    createdAt: string;
    updatedAt: string;
}