import type { StrapiMedia } from "../../StrapiMedia";

export interface Document {
    Title: string;
    Document: StrapiMedia;
    createdAt: string;
    updatedAt: string;
}