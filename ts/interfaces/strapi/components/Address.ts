export interface Address {
    id: number;
    Street: string;
    Number?: string;
    Zip: string;
    City: string;
    Country?: string;
    MapsEmbedUrl?: string;
}