
export interface Pagination<T> {
    // data: StrapiDataItem<T>[] | null;
    meta: {
        pagination: {
            total: number;
            page: number;
            pageSize: number;
            pageCount: number;
        }
    }
}