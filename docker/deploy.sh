#!/bin/bash

# Stop and remove the container
docker compose -f $1 down
docker compose -f $1 pull
docker compose -f $1 up $2